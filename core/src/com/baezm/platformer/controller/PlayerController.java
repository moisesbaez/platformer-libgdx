package com.baezm.platformer.controller;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.math.Vector2;
import com.baezm.platformer.model.Player;

public class PlayerController {
    public static Player player;
    public static boolean grounded;

    public static String movementAction;
    public static String specialAction;

    private static final float VELOCITY = 0.3f;
    private static final float MAX_VELOCITY = 5f;
    private static final float JUMP_VELOCITY = 7f;

    private enum State {
        Idle, Walk, Jump, Duck, Climb, Hurt, Swim
    }
    private static State playerState;

    public static void initializeController() {
        player = new Player(new Vector2(3, 5), 70, 100, "img/aliens.png");
        playerState = State.Idle;
        movementAction = "";
        specialAction = "";
    }

    public static void draw(Batch spriteBatch) {
        player.draw(spriteBatch);
    }

    public static void update(float deltaTime) {
        handleInput();
        player.update(deltaTime);
    }

    public static void handleInput() {
        Vector2 velocity = player.physicsBody.getLinearVelocity();
        Vector2 position = player.physicsBody.getPosition();

        if(Math.abs(velocity.x) > MAX_VELOCITY) {
            velocity.x = Math.signum(velocity.x) * MAX_VELOCITY;
            player.physicsBody.setLinearVelocity(velocity.x, velocity.y);
        }

        if(movementAction.equalsIgnoreCase("right")) {
            player.physicsBody.applyLinearImpulse(VELOCITY, 0, position.x + (player.width / 2f), position.y + (player.height / 2f), true);
            player.direction = "right";
        }
        else if(movementAction.equalsIgnoreCase("left")) {
            player.physicsBody.applyLinearImpulse(-VELOCITY, 0, position.x + (player.width / 2f), position.y + (player.height / 2f), true);
            player.direction = "left";
        }

        if(specialAction.equalsIgnoreCase("jump") && grounded) {
            velocity.y = 0;
            player.physicsBody.setLinearVelocity(velocity.x, velocity.y);
            player.physicsBody.applyLinearImpulse(0, JUMP_VELOCITY, position.x + (player.width / 2f), position.y + (player.height / 2f), true);
            grounded = false;
        }

        if(!grounded && Math.abs(velocity.y) > 0.0f) {
            playerState = State.Jump;
        }
        else if(grounded && Math.abs(velocity.x) > 0.2f) {
            playerState = State.Walk;
        }
        else {
            playerState = State.Idle;
        }

        setCurrentAnimation();
    }

    private static void setCurrentAnimation() {
        if(player.direction.equals("right")) {
            setRightAnimation();
        }
        else if(player.direction.equals("left")){
            setLeftAnimation();
        }
    }

    private static void setLeftAnimation() {
        if(playerState == State.Walk) {
            player.currentAnimation = "walkLeft";
        }
        else if(playerState == State.Idle) {
            player.currentAnimation = "idleLeft";
        }
        else if(playerState == State.Jump) {
            player.currentAnimation = "jumpLeft";
        }
    }

    private static void setRightAnimation() {
        if(playerState == State.Walk) {
            player.currentAnimation = "walkRight";
        }
        else if(playerState == State.Idle) {
            player.currentAnimation = "idleRight";
        }
        else if(playerState == State.Jump) {
            player.currentAnimation = "jumpRight";
        }
    }
}
