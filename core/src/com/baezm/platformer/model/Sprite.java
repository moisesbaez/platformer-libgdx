package com.baezm.platformer.model;

import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.baezm.platformer.controller.LevelController;

import java.util.HashMap;

public class Sprite {
    public Body physicsBody;
    public Vector2 position;
    public String direction;

    public float width;
    public float height;

    public Spritesheet spriteSheet;
    public String currentAnimation;
    protected float stateTime;
    protected HashMap<String, Animation> animations;

    public float rotation;

    public Sprite(Vector2 position, int width, int height, String sheetPath) {
        this.position = position;
        direction = "right";
        spriteSheet = new Spritesheet(sheetPath, width, height);
        this.width = width * LevelController.UNIT_SCALE;
        this.height = height * LevelController.UNIT_SCALE;
        animations = new HashMap<String, Animation>();
        stateTime = 0f;
    }

    public void draw(Batch spriteBatch) {
        spriteBatch.draw(animations.get(currentAnimation).getKeyFrame(stateTime, true), position.x, position.y, width, height);
    }

    public void update(float deltaTime) {
        stateTime += deltaTime;
    }
}
