package com.baezm.platformer.controller;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector3;

public class CameraController {
    public static OrthographicCamera camera;
    public static OrthographicCamera inputCamera;

    public static float widthScale;
    public static float heightScale;

    public static void initializeController() {
        float width = Gdx.graphics.getWidth();
        float height = Gdx.graphics.getHeight();
        camera = new OrthographicCamera(14f, 14f * (height / width));
        inputCamera = new OrthographicCamera(14f, 14f * (height / width));
        inputCamera.position.set(inputCamera.viewportWidth / 2, inputCamera.viewportHeight / 2, 0);
        inputCamera.update();
    }

    public static void update() {
        camera.position.set(clampCameraToMap());
        camera.update();
    }

    public static void resize(int width, int height) {
        camera.viewportWidth = 14f;
        camera.viewportHeight = 14f * height / width;
        camera.update();

        inputCamera.viewportWidth = 14f;
        inputCamera.viewportHeight = 14f * height / width;
        inputCamera.position.set(inputCamera.viewportWidth / 2, inputCamera.viewportHeight / 2, 0);
        inputCamera.update();

        widthScale = Gdx.graphics.getWidth() / inputCamera.viewportWidth * LevelController.UNIT_SCALE;
        heightScale = Gdx.graphics.getHeight() / inputCamera.viewportHeight * LevelController.UNIT_SCALE;

    }

    private static Vector3 clampCameraToMap() {
        float xPosition = PlayerController.player.position.x;
        float yPosition = PlayerController.player.position.y;

        xPosition = MathUtils.clamp(xPosition, camera.viewportWidth / 2f, LevelController.level.width - (camera.viewportWidth / 2f));
        yPosition = MathUtils.clamp(yPosition, camera.viewportHeight / 2f, LevelController.level.height - (camera.viewportHeight / 2f));
        return new Vector3(xPosition, yPosition, 0);
    }
}
