package com.baezm.platformer.model;

import com.badlogic.gdx.maps.MapLayer;
import com.badlogic.gdx.maps.MapObjects;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.maps.tiled.TmxMapLoader;
import com.baezm.platformer.controller.LevelController;

public class Level {
    public TiledMap map;
    public float width;
    public float height;

    public Level(String mapPath) {
        map = new TmxMapLoader().load(mapPath);
        width = Float.valueOf(map.getProperties().get("width").toString());
        height = Float.valueOf(map.getProperties().get("height").toString());
    }

    public MapLayer getMapLayer(String layerName) {
        return map.getLayers().get(layerName);
    }

    public MapObjects getLayerObjects(MapLayer mapLayer) {
        return mapLayer.getObjects();
    }

}
