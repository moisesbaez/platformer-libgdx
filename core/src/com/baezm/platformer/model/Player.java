package com.baezm.platformer.model;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.baezm.platformer.controller.LevelController;

public class Player extends Sprite {

    public Player(Vector2 position, int width, int height, String sheetPath) {
        super(position, width, height, "img/aliens.png");
        BodyDef bodyDefinition = new BodyDef();
        bodyDefinition.type = BodyDef.BodyType.DynamicBody;
        bodyDefinition.position.set(position.x + (this.width / 2f), position.y + (this.height / 2f));

        physicsBody = LevelController.gameWorld.createBody(bodyDefinition);
        physicsBody.setUserData(this);
        physicsBody.setFixedRotation(true);

        PolygonShape rectangleShape = new PolygonShape();
        rectangleShape.setAsBox(this.width / 2, this.height / 2, new Vector2(this.width / 2, this.height / 2), 0f);

        PolygonShape sensorShape = new PolygonShape();
        sensorShape.setAsBox(this.width / 2.1f, this.height / 64, new Vector2(this.width / 2, 0), 0f);

        FixtureDef boundingBox = new FixtureDef();
        boundingBox.shape = rectangleShape;
        boundingBox.density = 1f;
        boundingBox.filter.categoryBits = (short)LevelController.CollisionType.Player.Bits;
        boundingBox.filter.maskBits = (short)(LevelController.CollisionType.Environment.Bits | LevelController.CollisionType.Enemy.Bits);

        FixtureDef groundSensor = new FixtureDef();
        groundSensor.shape = sensorShape;
        groundSensor.isSensor = true;

        physicsBody.createFixture(boundingBox);
        physicsBody.createFixture(groundSensor);
        rectangleShape.dispose();
        sensorShape.dispose();

        animations.put("walkRight", spriteSheet.createAnimation(9, 10, 0.25f));
        animations.put("walkLeft", spriteSheet.flipAnimation(animations.get("walkRight"), true, false));

        animations.put("climbRight", spriteSheet.createAnimation(1, 2, 0.25f));
        animations.put("climbLeft", spriteSheet.flipAnimation(animations.get("climbRight"), true, false));

        animations.put("duckRight", spriteSheet.createAnimation(3, 3, 0.25f));
        animations.put("duckLeft", spriteSheet.flipAnimation(animations.get("duckRight"), true, false));

        animations.put("hurtRight", spriteSheet.createAnimation(4, 4, 0.25f));
        animations.put("hurtLeft", spriteSheet.flipAnimation(animations.get("hurtRight"), true, false));

        animations.put("jumpRight", spriteSheet.createAnimation(5, 5, 0.25f));
        animations.put("jumpLeft", spriteSheet.flipAnimation(animations.get("jumpRight"), true, false));

        animations.put("idleRight", spriteSheet.createAnimation(6, 6, 0.25f));
        animations.put("idleLeft", spriteSheet.flipAnimation(animations.get("idleRight"), true, false));

        animations.put("swimRight", spriteSheet.createAnimation(7, 8, 0.5f));
        animations.put("swimLeft", spriteSheet.flipAnimation(animations.get("swimRight"), true, false));

        currentAnimation = "walkRight";
    }

    public void draw(Batch spriteBatch) {
        super.draw(spriteBatch);
    }

    public void update(float deltaTime) {
        super.update(deltaTime);
    }
}
