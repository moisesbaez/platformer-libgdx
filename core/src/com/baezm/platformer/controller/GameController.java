package com.baezm.platformer.controller;

import com.badlogic.gdx.Game;
import com.baezm.platformer.view.GameScreen;
import com.baezm.platformer.view.TitleScreen;

public class GameController {
    private static Game game;

    public static void initializeController(Game currentGame) {
        game = currentGame;
        game.setScreen(new TitleScreen());
    }

    public static void startGame() {
        game.setScreen(new GameScreen("map/level01.tmx"));
    }
}

