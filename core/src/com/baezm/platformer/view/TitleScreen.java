package com.baezm.platformer.view;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.baezm.platformer.controller.GameController;

public class TitleScreen implements Screen {
    private SpriteBatch spriteBatch;
    private TextureRegion titleScreen;
    private int imageWidth;
    private int imageHeight;

    public TitleScreen() {
        spriteBatch = new SpriteBatch();
        Texture texture = new Texture(Gdx.files.internal("img/title-screen.png"));
        titleScreen = new TextureRegion(texture, 0, 0, texture.getWidth(), texture.getHeight());
        imageWidth = Gdx.graphics.getWidth();
        imageHeight = Gdx.graphics.getHeight();
    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor(0f, 0f, 0f, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        spriteBatch.begin();
        spriteBatch.draw(titleScreen, 0, 0, imageWidth, imageHeight);
        spriteBatch.end();

        if(Gdx.input.isTouched()) {
            GameController.startGame();
        }

    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void show() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void dispose() {

    }
}
