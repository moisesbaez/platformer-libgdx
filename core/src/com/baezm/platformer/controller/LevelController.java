package com.baezm.platformer.controller;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.maps.MapObject;
import com.badlogic.gdx.maps.MapObjects;
import com.badlogic.gdx.maps.tiled.renderers.OrthogonalTiledMapRenderer;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.Box2DDebugRenderer;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.utils.Array;
import com.baezm.platformer.model.Bodies;
import com.baezm.platformer.model.CollisionListener;
import com.baezm.platformer.model.Level;
import com.baezm.platformer.model.Sprite;

import java.util.ArrayList;


public class LevelController {
    public static final float UNIT_SCALE = 1/70f;

    public static Level level;
    public static OrthogonalTiledMapRenderer renderer;

    public static Batch spriteBatch;

    public static ArrayList<Sprite> gameSprites;

    public static World gameWorld;
    private static Array<Body> worldBodies;
    private static Box2DDebugRenderer debugRenderer;

    public static enum CollisionType {
        Environment (1),
        Player (1 << 1),
        Enemy (1 << 2);

        public int Bits;

        CollisionType(int hexValue) {
            Bits = hexValue;
        }
    }

    public static void initializeController(String mapPath) {
        level = new Level(mapPath);
        renderer = new OrthogonalTiledMapRenderer(level.map, UNIT_SCALE);

        gameWorld = new World(new Vector2(0, -10), true);
        gameWorld.setContactListener(new CollisionListener());
        worldBodies = new Array<Body>();
        debugRenderer = new Box2DDebugRenderer();

        spriteBatch = renderer.getSpriteBatch();
        gameSprites = new ArrayList<Sprite>();

        createLevelBodies();
        InputController.initializeController();
    }

    public static void draw() {
        spriteBatch.setProjectionMatrix(CameraController.camera.combined);
        spriteBatch.begin();
        PlayerController.draw(spriteBatch);
        for(Sprite sprite : gameSprites) {
            sprite.draw(spriteBatch);
        }
        spriteBatch.end();
        spriteBatch.setProjectionMatrix(CameraController.inputCamera.combined);
        InputController.draw(spriteBatch);
        debugRenderer.render(gameWorld, CameraController.camera.combined);
    }

    public static void update(float deltaTime) {
        renderer.setView(CameraController.camera);
        renderer.render();
        updateWorldBodies();
        gameWorld.step(1/60f, 1, 1);
    }

    private static void updateWorldBodies() {
        worldBodies.clear();
        gameWorld.getBodies(worldBodies);

        for(Body body : worldBodies) {
            Sprite spriteBody = (Sprite)body.getUserData();

            if(spriteBody != null) {
                spriteBody.position = body.getPosition();
                spriteBody.rotation = (float)Math.toDegrees(body.getAngle());
            }
        }
    }

    private static void createLevelBodies() {
        MapObjects mapObjects = level.getLayerObjects(level.getMapLayer("collision"));

        for(MapObject mapObject : mapObjects) {
            Bodies.createBody(mapObject);
        }
    }
}
