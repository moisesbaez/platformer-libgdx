package com.baezm.platformer.controller;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.InputAdapter;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.baezm.platformer.model.InputControl;
import com.baezm.platformer.model.Spritesheet;

import java.util.ArrayList;

public class InputController {
    private static ArrayList<InputControl> inputControls;
    private static Spritesheet spriteSheet;

    public static void initializeController() {
        inputControls = new ArrayList<InputControl>();
        spriteSheet = new Spritesheet("img/touch-controls.png", 80, 80);
        InputControl left = new InputControl(new Vector2(0.5f, 0.5f), spriteSheet.spriteFrames[0], "left");
        InputControl right = new InputControl(new Vector2(left.position.x + 1.3f, 0.5f), spriteSheet.spriteFrames[1], "right");
        InputControl jump = new InputControl(new Vector2(CameraController.inputCamera.viewportWidth - 1.8f, 0.5f), spriteSheet.spriteFrames[12], "jump");
        inputControls.add(left);
        inputControls.add(right);
        inputControls.add(jump);
        Gdx.input.setInputProcessor(createInputAdapter());
    }

    public static void draw(Batch spriteBatch) {
        spriteBatch.begin();
        for(InputControl inputControl : inputControls) {
            inputControl.draw(spriteBatch);
        }
        spriteBatch.end();
    }

    private static InputAdapter createInputAdapter() {
        return new InputAdapter() {
            @Override
            public boolean touchDown(int screenX, int screenY, int pointer, int button) {
                screenY = Gdx.graphics.getHeight() - screenY;
                for(InputControl inputControl : inputControls) {
                    if(inputControl.getBoundingBox().contains(screenX, screenY)) {
                        if(inputControl.action.equalsIgnoreCase("left") || inputControl.action.equalsIgnoreCase("right")) {
                            PlayerController.movementAction = inputControl.action;
                        }
                        else if(inputControl.action.equalsIgnoreCase("jump")) {
                            PlayerController.specialAction = inputControl.action;
                        }
                    }
                }
                return true;
            }

            @Override
            public boolean touchUp(int screenX, int screenY, int pointer, int button) {
                screenY = Gdx.graphics.getHeight() - screenY;
                for(InputControl inputControl : inputControls) {
                    if(inputControl.getBoundingBox().contains(screenX, screenY)) {
                        if(inputControl.action.equalsIgnoreCase("left") || inputControl.action.equalsIgnoreCase("right")) {
                            PlayerController.movementAction = "";
                        }
                        else if(inputControl.action.equalsIgnoreCase("jump")) {
                            PlayerController.specialAction = "";
                        }
                    }
                }
                return true;
            }

            @Override
            public boolean keyDown(int keycode) {
                if(keycode == Input.Keys.RIGHT) {
                    PlayerController.movementAction = "right";
                }
                else if(keycode == Input.Keys.LEFT) {
                    PlayerController.movementAction = "left";
                }
                else if(keycode == Input.Keys.SPACE) {
                    PlayerController.specialAction = "jump";
                }
                return true;
            }

            @Override
            public boolean keyUp(int keycode) {
                if(keycode == Input.Keys.RIGHT || keycode == Input.Keys.LEFT) {
                    PlayerController.movementAction = "";
                }
                else if(keycode == Input.Keys.SPACE) {
                    PlayerController.specialAction = "";
                }
                return true;
            }
        };
    }
}
