package com.baezm.platformer.model;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.math.Vector2;

public class Block extends Sprite {

    public Block(Vector2 position, int spriteFrame) {
        super(position, 70, 70, "img/background-tiles.png");
        animations.put("block", spriteSheet.createAnimation(spriteFrame, spriteFrame, 0));
        currentAnimation = "block";
        rotation = 0;
    }

    @Override
    public void draw(Batch spriteBatch) {
        spriteBatch.draw(animations.get(currentAnimation).getKeyFrame(stateTime, false), position.x, position.y, 0, 0, width, height, 1.0f, 1.0f, rotation);
    }
}
